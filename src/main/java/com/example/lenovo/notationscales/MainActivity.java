package com.example.lenovo.notationscales;

import android.renderscript.Sampler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.binary)
    TextView binary;
    @BindView(R.id.octo)
    TextView octo;
    @BindView(R.id.hex)
    TextView hex;

    @BindView(R.id.input_for_binary)
    EditText input_for_binary;
    @BindView(R.id.input_for_octo)
    EditText input_for_octo;
    @BindView(R.id.input_for_hex)
    EditText input_for_hex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnTextChanged(R.id.input_for_binary)
    public void onBinaryChanged() {
        String number = normalParse(input_for_binary.getText().toString());
        binary.setText("bin: "+Integer.toBinaryString(Integer.parseInt(number)));
    }

    @OnTextChanged(R.id.input_for_octo)
    public void onOctoChanged() {
        String number = normalParse(input_for_octo.getText().toString());
        octo.setText("octo: "+Integer.toOctalString(Integer.parseInt(number)));
    }

    @OnTextChanged(R.id.input_for_hex)
    public void onHexChanged() {
        String number = normalParse(input_for_hex.getText().toString());
        hex.setText("hex: "+Integer.toHexString(Integer.parseInt(number)));
    }

    public String normalParse(String number){
        if(number.equalsIgnoreCase("") || number.length() > 9){
            number = "0";
        }
        return number;
    }
}
